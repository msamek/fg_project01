# First exercise - Servlets

**Running Application**:
mvn spring-boot:run (spring-boot:run -Drun.arguments=--key_param=value_param)

**Testing application**:
mvn test

#### **CLI Parameters**
Parameter to override application.properties configuration file


    --app.configuration.file=#REPLACEME#

#### **Configuration file template**
Configuration file for file database


    <?xml version="1.0" encoding="UTF-8"?>
    <configuration>
        <dbType>file</dbType>
        <nameDaysFile>#REPLACEME#</nameDaysFile>
    </configuration>


Configuration file for JDBC configuration (MySQL Example)

    <?xml version="1.0" encoding="UTF-8"?>
    <configuration>
        <dbType>relation</dbType>
        <driver>com.mysql.jdbc.Driver</driver>
        <connectionstring>jdbc:mysql://localhost:3306/#SCHEMA-REPLACEME#</connectionstring>
        <username>#REPLACEME#</username>
        <password>#REPLACEME#</password>
    </configuration>


### REQUEST EXAMPLES

**POST**


Change the whole content of database

    curl --request POST \
      --url http://localhost:8080/test \
      --header 'content-type: application/json' \
      --data '{
        "05/27": "Valdemar",
        "05/28": "Vilem"
    }'

**GET**

Retrieve Today's date and the value of Name day, if exist.


    curl --request GET --url http://localhost:8080/test 



**Database**


    create table name_day
    (
        day varchar(5) not null
            primary key,
        name varchar(50) not null
    );

