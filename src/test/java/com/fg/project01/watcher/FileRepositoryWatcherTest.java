package com.fg.project01.watcher;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static junit.framework.TestCase.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FileRepositoryWatcherTest {

    private CountDownLatch lock = new CountDownLatch(1);

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void OnFileChange_NewFileCreated_ActiveWait_FileCreated() throws Exception {
        String tempFileName = "test.json";
        Path fileToWatch = getFileToWatch(tempFileName);

        AtomicReference<Boolean> finished = new AtomicReference<>(false);
        FileRepositoryWatcher.onFileChange(fileToWatch, fileExist -> {
            finished.set(fileExist);
        });

        createTempFile(tempFileName);

        int limit = 10;
        while (!finished.get()) {
            Thread.sleep(1000);
            if (--limit <= 0) {
                throw new Exception("Test failed on timelimit");
            }
        }

        assertTrue(finished.get());
    }

    @Test
    public void OnFileChange_NewFileCreated_withLock_FileCreated() throws IOException, InterruptedException {
        String tempFileName = "test.json";
        Path fileToWatch = getFileToWatch(tempFileName);

        AtomicReference<Boolean> finished = new AtomicReference<>(false);
        FileRepositoryWatcher.onFileChange(fileToWatch, fileExist -> {
            assertTrue(fileExist);
            finished.set(true);
        });

        createTempFile(tempFileName);

        lock.await(2000, TimeUnit.MILLISECONDS);
        assertTrue(finished.get());
    }

    @Test
    public void OnFileChange_NewFileCreated_WithFuture_FileCreated() throws IOException, InterruptedException, ExecutionException {
        String tempFileName = "test.json";
        Path fileToWatch = getFileToWatch(tempFileName);

        CompletableFuture<Boolean> future = new CompletableFuture<>();
        FileRepositoryWatcher.onFileChange(fileToWatch, fileExist -> {
            future.complete(fileExist);
        });

        createTempFile(tempFileName);

        assertTrue(future.get());
    }

    private Path getFileToWatch(String tempFileName) {
        Path path1 = Paths.get(tempFolder.getRoot().toURI());
        return path1.resolve(tempFileName);
    }

    private void createTempFile(String tempFileName) throws IOException {
        final File tempFile = tempFolder.newFile(tempFileName);
        Files.write(tempFile.toPath(),
                "YourString".getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
    }
}
