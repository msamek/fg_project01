package com.fg.project01.repository;

import com.fg.project01.exception.RepositoryException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionSystemException;

import java.util.HashMap;

import static org.mockito.Matchers.any;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class NameDayRepositoryTest {

    @Mock
    private PlatformTransactionManager transactionManager;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private NameDayRepository nameDayRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void GetNameDay_DateParam_Success() throws RepositoryException {
        nameDayRepository.getNameDay("05/05");

        Mockito.verify(jdbcTemplate, Mockito.times(1)).query(
                Mockito.contains("select name from name_day where day='05/05'"), Mockito.any(ResultSetExtractor.class));
    }

    @Test(expected = RepositoryException.class)
    public void Configure_NullParam_RaisesRepositoryException() throws RepositoryException {
        nameDayRepository.configure(null);

        Mockito.verify(jdbcTemplate, Mockito.times(0)).update(Mockito.any(String.class));
    }

    @Test
    public void Configure_OneParam_Success() throws RepositoryException {
        HashMap<String, String> nameDays = new HashMap<>();
        nameDays.put("05/05", "Pavel");

        nameDayRepository.configure(nameDays);

        Mockito.verify(jdbcTemplate, Mockito.times(1)).update(
                Mockito.contains("DELETE FROM name_day"));
        Mockito.verify(jdbcTemplate, Mockito.times(1)).update(
                Mockito.contains("INSERT INTO name_day (day, name) VALUES (?, ?)"), Mockito.contains("05/05"), Mockito.contains("Pavel"));
        Mockito.verify(transactionManager, Mockito.times(1)).commit(any());
    }

    @Test(expected = RepositoryException.class)
    public void Configure_TransactionFailed_RaisesRepositoryException() throws RepositoryException {
        Mockito.doThrow(new TransactionSystemException("Transaction not saved")).when(transactionManager).commit(any());
        HashMap<String, String> nameDays = new HashMap<>();
        nameDays.put("05/05", "Pavel");

        nameDayRepository.configure(nameDays);

        Mockito.verify(jdbcTemplate, Mockito.times(1)).update(
                Mockito.contains("DELETE FROM name_day"));
        Mockito.verify(jdbcTemplate, Mockito.times(1)).update(
                Mockito.contains("INSERT INTO name_day (day, name) VALUES (?, ?)"), Mockito.contains("05/05"), Mockito.contains("Pavel"));
        Mockito.verify(transactionManager, Mockito.times(1)).commit(any());
        Mockito.verify(transactionManager, Mockito.times(1)).rollback(any());
    }

}
