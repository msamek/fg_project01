package com.fg.project01.repository;

import com.fg.project01.configuration.AppConfiguration;
import com.fg.project01.exception.RepositoryException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class NameDayFileRepositoryTest {
    private static final String TEMP_FILE = "src/test/resources/name_day_test_runtime.json";

    @InjectMocks
    private NameDayFileRepository nameDayRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterClass
    public static void cleanup() throws IOException {
        removeFile(TEMP_FILE);
    }

    @Test
    public void GetNameDay_KeyExist_Success() throws RepositoryException, IOException, InterruptedException {
        nameDayRepository.setFilePath("src/test/resources/name_day_test.json");
        String actualName = nameDayRepository.getNameDay("05/05");

        assertEquals(actualName, "Whatever");
    }

    @Test
    public void GetNameDay_KeyNotExist_ReturnNull() throws RepositoryException, IOException {
        nameDayRepository.setFilePath("src/test/resources/name_day_test.json");
        String actualName = nameDayRepository.getNameDay("05/06");

        assertNull(actualName);
    }

    @Test(expected = RepositoryException.class)
    public void Configure_ParamNull_RaiseRepositoryException() throws RepositoryException, IOException {
        nameDayRepository.setFilePath("src/test/resources/name_day_test_temp.json");
        nameDayRepository.configure(null);
    }

    @Test(expected = RepositoryException.class)
    public void Configure_ParamEmptyMap_RaiseRepositoryException() throws RepositoryException, IOException {
        nameDayRepository.setFilePath("src/test/resources/name_day_test_temp.json");
        nameDayRepository.configure(new HashMap<>());
    }

    @Test()
    public void Configure_OneRecord_CreateFileAndRead() throws RepositoryException, IOException {
        String filename = AppConfiguration.getAbsolutePath(TEMP_FILE);
        nameDayRepository.setFilePath(filename);
        HashMap<String, String> data = new HashMap<>();
        data.put("05/05", "NameDay");

        nameDayRepository.configure(data);

        //Validation
        String actualResult = nameDayRepository.getNameDay("05/05");
        assertEquals(actualResult, "NameDay");
    }

    private static void removeFile(String filename) throws IOException {
        Files.deleteIfExists(Paths.get(filename));
    }
}
