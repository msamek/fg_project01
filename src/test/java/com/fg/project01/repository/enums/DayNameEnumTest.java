package com.fg.project01.repository.enums;

import lombok.var;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DayNameEnumTest {

    @Test
    public void FromString_ParamNotExist_ReturnsInvalidEnum() {
        var actualResult = DayNameEnum.fromString("DayNotExist");

        assertEquals(DayNameEnum.INVALID , actualResult);
    }

    @Test
    public void FromString_ParamTue_ReturnsTuesday() {
        var actualResult = DayNameEnum.fromString("TUE");

        assertEquals(DayNameEnum.TUESDAY , actualResult);
    }
}
