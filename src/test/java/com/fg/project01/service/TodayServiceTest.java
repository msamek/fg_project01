package com.fg.project01.service;

import com.fg.project01.repository.enums.DayNameEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TodayServiceTest {

    private TodayService todayService;

    @Before
    public void setUp(){
        todayService = new TodayService();

    }

    @Test
    public void GetToday_NoParams_EqualsToday() {
        Date expectedResult = new Date();
        Date actualResult = todayService.getToday();

        assertEquals(todayService.format(expectedResult), todayService.format(actualResult));
    }

    @Test
    public void Format_DayToday_Success() {
        String expectedResult = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).format(new Date());
        String actualResult = todayService.format(todayService.getToday());

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void NameDay_CustomDate_Success() throws ParseException {
        Date thursday = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse("28/05/2020");

        DayNameEnum actualResult = todayService.toDayName(thursday);

        assertEquals(DayNameEnum.THURSDAY, actualResult);
    }

    @Test
    public void NameDay_NullParam_InvalidResult() {
        DayNameEnum actualResult = todayService.toDayName(null);

        assertEquals(DayNameEnum.INVALID, actualResult);
    }

    @Test
    public void NameDay_CustomDateCzechLocale_Success() throws ParseException {
        Locale.setDefault(new Locale("cs", "CZ"));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date thursday = simpleDateFormat.parse("28/05/2020");
        assertEquals("Čt", new SimpleDateFormat("EEE").format(thursday));

        DayNameEnum actualResult = todayService.toDayName(thursday);

        assertEquals(DayNameEnum.THURSDAY, actualResult);
    }
}
