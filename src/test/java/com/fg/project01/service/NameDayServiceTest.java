package com.fg.project01.service;

import com.fg.project01.exception.RepositoryException;
import com.fg.project01.exception.ValidationException;
import com.fg.project01.repository.INameDayRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class NameDayServiceTest {

    @Mock
    private INameDayRepository nameDayRepository;

    @InjectMocks
    private NameDayService nameDayService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = ValidationException.class)
    public void NameDay_EachMonthDayOverflow_ValidationExceptionThrown() throws ValidationException, RepositoryException {
        nameDayService.nameDay("01/32");
        nameDayService.nameDay("02/29");
        nameDayService.nameDay("03/32");
        nameDayService.nameDay("04/31");
        nameDayService.nameDay("05/32");
        nameDayService.nameDay("06/31");
        nameDayService.nameDay("07/32");
        nameDayService.nameDay("08/32");
        nameDayService.nameDay("09/31");
        nameDayService.nameDay("10/32");
        nameDayService.nameDay("11/31");
        nameDayService.nameDay("12/32");
    }

    @Test(expected = ValidationException.class)
    public void NameDay_DayFormatInvalid_ValidationExceptionThrown() throws ValidationException, RepositoryException {
        nameDayService.nameDay("12-12");
    }

    @Test(expected = ValidationException.class)
    public void NameDay_ParameterNotDay_ValidationExceptionThrown() throws ValidationException, RepositoryException {
        nameDayService.nameDay("FGForrest");
    }


    @Test(expected = ValidationException.class)
    public void NameDay_DayIsZero_ValidationExceptionThrown() throws ValidationException, RepositoryException {
        nameDayService.nameDay("00/01");
    }

    @Test()
    public void NameDay_DayNotExistInDatabase_Null() throws ValidationException, RepositoryException {
        String actualResult = nameDayService.nameDay("01/01");

        Mockito.verify(nameDayRepository, Mockito.times(1)).getNameDay("01/01");
        assertNull(actualResult);
    }

    @Test()
    public void NameDay_DayExist_Null() throws ValidationException, RepositoryException {
        String day = "02/01";
        Mockito.when(nameDayRepository.getNameDay(day)).thenReturn("Forrest");

        String actualResult  = nameDayService.nameDay(day);

        Mockito.verify(nameDayRepository, Mockito.times(1)).getNameDay(day);
        assertEquals("Forrest" , actualResult);
    }

    @Test(expected = ValidationException.class)
    public void Configure_InvalidJson_ValidationExpectionThrown() throws ValidationException, RepositoryException {
        BufferedReader reader = new BufferedReader(new StringReader("JustAnRandomText"));
        nameDayService.configure(reader);
    }

    @Test(expected = ValidationException.class)
    public void Configure_InvalidNameDayJson_ValidationExpectionThrown() throws ValidationException, RepositoryException {
        BufferedReader reader = new BufferedReader(new StringReader("{\"05/27\" : {\"Object\": []}}"));
        nameDayService.configure(reader);
    }

    @Test
    public void Configure_InvalidNameDayJson1_ValidationExpectionThrown() throws ValidationException, RepositoryException {
        BufferedReader reader = new BufferedReader(new StringReader("{\"05/27\" : \"Pavel\"}"));

        nameDayService.configure(reader);

        Mockito.verify(nameDayRepository, Mockito.times(1)).configure(any());
    }
}
