package com.fg.project01.watcher;

import com.fg.project01.exception.RepositoryException;

import java.io.IOException;

public interface Callback {
    void run(boolean fileExist) throws RepositoryException, IOException;
}