package com.fg.project01.watcher;

import com.fg.project01.exception.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.*;

public class FileRepositoryWatcher {

    private static final Logger LOG = LoggerFactory.getLogger(FileRepositoryWatcher.class);
    private Thread thread;
    private WatchService watchService;

    public static void onFileChange(Path file, Callback callback) throws IOException {
        FileRepositoryWatcher fileWatcher = new FileRepositoryWatcher();
        fileWatcher.start(file, callback);
        Runtime.getRuntime().addShutdownHook(new Thread(fileWatcher::stop));
    }

    public void start(Path file, Callback callback) throws IOException {
        Path parent = file.getParent();

        if (!Files.exists(parent)) {
            return;
        }

        watchService = FileSystems.getDefault().newWatchService();
        parent.register(watchService,
                StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE);

        thread = new Thread(() -> {
            while (true) {
                watch(file, callback, parent);
            }
        });
        thread.start();
    }

    public void stop() {
        if (thread == null) {
            return;
        }

        thread.interrupt();
        try {
            watchService.close();
        } catch (IOException e) {
            LOG.info("Error closing file watch service", e);
        }
    }

    private void watch(Path file, Callback callback, Path parent) {
        WatchKey wk = null;
        try {
            wk = watchService.take();
            Thread.sleep(500);
            checkPollEvents(file, callback, parent, wk);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (RepositoryException | IOException e) {
            LOG.error("Error while running a callback {}", e.getMessage());
        } finally {
            if (wk != null) {
                wk.reset();
            }
        }
    }

    private void checkPollEvents(Path file, Callback callback, Path parent, WatchKey wk) throws IOException, RepositoryException {
        for (WatchEvent<?> event : wk.pollEvents()) {
            try {
                Path changed = parent.resolve((Path) event.context());
                if (Files.exists(changed) && Files.isSameFile(changed, file)) {
                    LOG.info("File has been changed on file system: {}", changed);
                    callback.run(true);
                    return;
                }
            } catch (FileNotFoundException | NoSuchFileException e) {
                LOG.info("File was not found / has been removed {}", file);
                callback.run(false);
                return;
            }
        }
    }
}
