package com.fg.project01.repository;

import com.fg.project01.exception.RepositoryException;

import java.util.Map;

public interface INameDayRepository {

    String getNameDay(String day) throws RepositoryException;
    void configure(Map<String, String> data) throws RepositoryException;
    void configureV2(Map<String, String> data) throws RepositoryException;
}
