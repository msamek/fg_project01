package com.fg.project01.repository;

import com.fg.project01.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Map;

@Repository
public class NameDayRepository implements INameDayRepository {

    private JdbcTemplate jdbcTemplate;
    private PlatformTransactionManager transactionManager;

    @Autowired
    public NameDayRepository(JdbcTemplate jdbcTemplate, PlatformTransactionManager transactionManager) {
        this.jdbcTemplate = jdbcTemplate;
        this.transactionManager = transactionManager;
    }

    @Override
    public String getNameDay(String day) throws RepositoryException {
        try {
            return queryNameDay(day);
        } catch (Throwable t) {
            throw new RepositoryException(t.getMessage());
        }
    }

    @Override
    public void configure(Map<String, String> data) throws RepositoryException {
        if (data == null || data.isEmpty()) {
            throw new RepositoryException("Saving empty data is not allowed");
        }
        TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
        try {
            removeRows();
            for (Map.Entry<String, String> entry : data.entrySet()) {
                addRow(entry.getKey(), entry.getValue());
            }
            transactionManager.commit(txStatus);
        } catch (Throwable t) {
            transactionManager.rollback(txStatus);
            throw new RepositoryException(t.getMessage());
        }
    }

    @Override
    @Transactional
    public void configureV2(Map<String, String> data) throws RepositoryException {
        if (data == null || data.isEmpty()) {
            throw new RepositoryException("Saving empty data is not allowed");
        }

        removeRows();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            addRow(entry.getKey(), entry.getValue());
        }
        //  Pokud bych nechal catch throwable, tak by transactional prestal fungovat
        //        try {
        //            removeRows();
        //            for (Map.Entry<String, String> entry : data.entrySet()) {
        //                addRow(entry.getKey(), entry.getValue());
        //            }
        //        } catch (Throwable t) {
        //            throw new RepositoryException(t.getMessage());
        //        }
    }

    private String queryNameDay(String day) {
        return jdbcTemplate.query("select name from name_day where day='" + day + "'",
                rs -> rs.next() ? rs.getString("name") : null
        );
    }

    private void removeRows() {
        jdbcTemplate.update("DELETE FROM name_day");
    }

    private void addRow(String date, String name) {
        jdbcTemplate.update("INSERT INTO name_day (day, name) VALUES (?, ?)", date, name);
    }
}
