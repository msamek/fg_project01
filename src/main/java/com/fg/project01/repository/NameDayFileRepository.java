package com.fg.project01.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fg.project01.exception.RepositoryException;
import com.fg.project01.watcher.FileRepositoryWatcher;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Repository
public class NameDayFileRepository implements INameDayRepository {

    private static final Logger LOG = LoggerFactory.getLogger(NameDayFileRepository.class);
    private Map<String, String> nameDays;
    private String filePath;

    public void setFilePath(String filePath) throws IOException {
        this.filePath = filePath;
        FileRepositoryWatcher.onFileChange(Paths.get(this.filePath), fileExist -> {
            this.load(fileExist);
        });
    }

    /**
     * Method retrieves the name day for a day in format MM/dd, late initialization for reading from file is applied
     *
     * @param day
     * @return
     */
    public String getNameDay(String day) throws RepositoryException {
        if (nameDays == null) {
            try {
                load(true);
            } catch (IOException e) {
                throw new RepositoryException(e.getMessage());
            }
        }
        return nameDays.get(day);
    }

    /**
     * Method replace the database file with input structure for name days
     *
     * @param data
     */
    public void configure(Map<String, String> data) throws RepositoryException {
        this.saveNameDays(data);
    }

    @Override
    public void configureV2(Map<String, String> data) throws RepositoryException {
        throw new RepositoryException("Not for file");
    }

    private void saveNameDays(Map<String, String> data) throws RepositoryException {
        if (data == null || data.isEmpty()) {
            throw new RepositoryException("Saving empty data is not allowed");
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            File resultFile = new File(this.filePath);
            mapper.writeValue(resultFile, data);
        } catch (IOException e) {
            throw new RepositoryException(e.getMessage());
        }
    }

    private void load(boolean fileExist) throws RepositoryException, IOException {
        this.nameDays = null;
        if (!fileExist) {
            return;
        }

        LOG.info("Loading a {} file from file system", this.filePath);
        Set<Map.Entry<String, JsonElement>> entrySet;
        try (JsonReader jsonReader = new JsonReader(new FileReader(this.filePath))) {
            entrySet = new JsonParser().parse(jsonReader).getAsJsonObject().entrySet();
        }
        if (entrySet.isEmpty()) {
            throw new RepositoryException(String.format("Configuration file %s is empty", this.filePath));
        }
        nameDays = new HashMap<>();
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            nameDays.put(entry.getKey(), entry.getValue().getAsString());
        }
    }
}
