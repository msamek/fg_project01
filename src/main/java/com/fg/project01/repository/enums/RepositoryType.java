package com.fg.project01.repository.enums;

import lombok.Getter;

@Getter
public enum RepositoryType {
    FILE("file"),
    RELATION("relation");

    private final String text;

    RepositoryType(String text) {
        this.text = text;
    }

    public static RepositoryType fromString(String text) {
        for (RepositoryType b : RepositoryType.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }

        return null;
    }

}
