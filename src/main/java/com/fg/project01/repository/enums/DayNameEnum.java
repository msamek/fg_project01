package com.fg.project01.repository.enums;

public enum DayNameEnum {
    MONDAY("MON","Pondělí"),
    TUESDAY("TUE","Úterý"),
    WEDNESDAY("WED", "Středa"),
    THURSDAY("THU", "Čtvrtek"),
    FRIDAY("FRI", "Patek"),
    SATURDAY("SAT", "Sobota"),
    SUNDAY("SUN", "Neděle"),
    INVALID("-", "-");

    private String shortKey;
    private String translation;

    DayNameEnum(String shortKey, String translation) {
        this.shortKey = shortKey;
        this.translation = translation;
    }

    public String getTranslation() {
        return translation;
    }

    public static DayNameEnum fromString(String text) {
        for (DayNameEnum b : DayNameEnum.values()) {
            if (b.shortKey.equalsIgnoreCase(text)) {
                return b;
            }
        }

        return DayNameEnum.INVALID;
    }
}
