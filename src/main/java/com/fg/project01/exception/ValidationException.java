package com.fg.project01.exception;

public class ValidationException extends Exception {

    public ValidationException(String message) {
        super(message);
    }
}
