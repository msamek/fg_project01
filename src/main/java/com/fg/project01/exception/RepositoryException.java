package com.fg.project01.exception;

public class RepositoryException extends Exception {

    public RepositoryException(String message) {
        super(message);
    }
}
