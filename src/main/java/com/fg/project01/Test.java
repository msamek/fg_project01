package com.fg.project01;

import com.fg.project01.exception.RepositoryException;
import com.fg.project01.exception.ValidationException;
import com.fg.project01.service.NameDayService;
import com.fg.project01.service.TodayService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet(
        name = "FG Example of Servlet",
        description = "Example Servlet Using Annotations",
        urlPatterns = {"/test"}
)
public class Test extends HttpServlet {

    @Autowired
    private TodayService todayService;

    @Autowired
    private NameDayService nameDayService;

    @Override
    protected void doGet(
            HttpServletRequest request,
            HttpServletResponse response) throws IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        Date today = todayService.getToday();
        try {
            out.println("Dnešní den je " + todayService.toDayName(today).getTranslation() +
                    " s datumem: " + todayService.format(today) +
                    " a svátek má: " + nameDayService.nameDay(today));
        } catch (ValidationException | RepositoryException e) {
            badRequestResponse(response, e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            nameDayService.configure(req.getReader());
            resp.setStatus(HttpServletResponse.SC_CREATED);
        } catch (ValidationException | RepositoryException e) {
            badRequestResponse(resp, e);
        }
    }

    private void badRequestResponse(HttpServletResponse resp, Exception e) throws IOException {
        PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        writer.println("{\"message\" : \"" + e.getMessage() + "\"}");
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
}