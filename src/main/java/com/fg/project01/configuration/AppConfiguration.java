package com.fg.project01.configuration;

import com.fg.project01.repository.INameDayRepository;
import com.fg.project01.repository.NameDayFileRepository;
import com.fg.project01.repository.NameDayRepository;
import com.fg.project01.repository.enums.RepositoryType;
import lombok.Getter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Getter
@EnableTransactionManagement
@Configuration
public class AppConfiguration {
    private static final String DEFAULT_DB_TYPE = "file";
    private static final String DEFAULT_JSON_FILE = "nameday.json";
    private static final String DEFAULT_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DEFAULT_CON_STRING = "jdbc:mysql://localhost:3306/project01";
    private static final String DEFAULT_USER = "root";
    private static final String DEFAULT_PASS = "";


    @Value("${app.configuration.file}")
    private String customConfFile;


    private Element xmlRoot;
    private RepositoryType dbType = RepositoryType.FILE;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @PostConstruct
    private void loadConfiguration() throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        if (new File(customConfFile).exists()) {
            Document document = saxBuilder.build(new FileReader(customConfFile));
            this.xmlRoot = document.getRootElement();
        }
        setAttributes();
    }

    private void setAttributes() {
        this.dbType = RepositoryType.fromString(getElementValue("dbType", DEFAULT_DB_TYPE));
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        if (dbType == RepositoryType.RELATION) {
            dataSource.setDriverClassName(getElementValue("driver", DEFAULT_DRIVER));
            dataSource.setUrl(getElementValue("connectionstring", DEFAULT_CON_STRING));
            dataSource.setUsername(getElementValue("username", DEFAULT_USER));
            dataSource.setPassword(getElementValue("password", DEFAULT_PASS));
        }

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return dbType == RepositoryType.RELATION ? new JdbcTemplate(dataSource) : null;
    }

    @Bean
    @Primary
    @ApplicationScope
    public INameDayRepository nameDayRepository() throws IOException {
        switch (this.dbType) {
            case FILE:
                NameDayFileRepository nameDayFileRepository = new NameDayFileRepository();
                nameDayFileRepository.setFilePath(getAbsolutePath(
                        getElementValue("nameDaysFile", DEFAULT_JSON_FILE)));
                return nameDayFileRepository;
            case RELATION:
                return new NameDayRepository(jdbcTemplate(dataSource()), transactionManager());
        }

        return null;
    }

    private String getElementValue(String key, String defaultValue) {
        return xmlRoot != null && xmlRoot.getChild(key) != null ? xmlRoot.getChild(key).getValue() : defaultValue;
    }

    public static String getAbsolutePath(String filePath) {
        Path path = Paths.get(filePath);
        if (!path.isAbsolute()) {
            Path currentPath = Paths.get(System.getProperty("user.dir"));
            path = Paths.get(currentPath.toString(), filePath);
        }
        return path.toString();
    }
}
