package com.fg.project01.service;

import com.fg.project01.exception.RepositoryException;
import com.fg.project01.exception.ValidationException;
import com.fg.project01.repository.INameDayRepository;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Service
public class NameDayService {

    private static final Logger LOG = LoggerFactory.getLogger(NameDayService.class);

    //Validates possible input dates
    private static final Pattern DAY_FORMAT = Pattern.compile(
            "((0[1-9]|1[0-2])/([01][1-9]|10|2[0-8]))|((0[13-9]|1[0-2])/(29|30))|((0[13578]|1[0-2])/31)");

    private INameDayRepository nameDayRepository;

    @Autowired
    public NameDayService(INameDayRepository nameDayFileRepository) {
        this.nameDayRepository = nameDayFileRepository;
    }

    /**
     * Function retrieves the name day for a date (Year is not used)
     */
    public String nameDay(Date date) throws ValidationException, RepositoryException {
        DateFormat formatter = new SimpleDateFormat("MM/dd", Locale.ENGLISH);
        return this.nameDay(formatter.format(date));
    }

    /**
     * Function retrieves the name day by MM/dd string format
     */
    public String nameDay(String date) throws ValidationException, RepositoryException {
        if (!DAY_FORMAT.matcher(date).matches()) {
            throw new ValidationException("The input date is string is not valid. Required format: MM/dd");
        }

        return nameDayRepository.getNameDay(date);
    }

    /***
     *  Method parses the buffered reader and sends request to repository to save new database content.
     */
    public void configure(BufferedReader bodyReader) throws ValidationException, RepositoryException {
        HashMap<String, String> nameDays = parseBodyBuffer(bodyReader);
        nameDayRepository.configure(nameDays);
    }

    private HashMap<String, String> parseBodyBuffer(BufferedReader reader) throws ValidationException {
        HashMap<String, String> nameDays = new HashMap<>();
        try {
            JsonElement parse = new JsonParser().parse(new JsonReader(reader));
            if (parse instanceof JsonObject) {
                Set<Map.Entry<String, JsonElement>> entries = parse.getAsJsonObject().entrySet();
                for (Map.Entry<String, JsonElement> entry : entries) {
                    addEntry(nameDays, entry);
                }
                return nameDays;
            }
        } catch (Throwable t) {
            LOG.info("Malformed JSON structure");
            LOG.debug(t.getMessage());
        }
        throw new ValidationException("Reader contains not valid JSON object structure");
    }

    private void addEntry(HashMap<String, String> nameDays, Map.Entry<String, JsonElement> entry) throws ValidationException {
        if (isEntryValid(entry.getKey(), entry.getValue())) {
            nameDays.put(entry.getKey(), entry.getValue().getAsString());
        } else {
            throw new ValidationException(
                    String.format("Structure for a record: \"%s\":%s is not valid", entry.getKey(), entry.getValue().toString()));
        }
    }

    private boolean isEntryValid(String key, JsonElement value) {
        return DAY_FORMAT.matcher(key).matches() && (value.isJsonPrimitive() && !value.getAsString().isEmpty());
    }

}
