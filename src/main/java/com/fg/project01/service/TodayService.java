package com.fg.project01.service;

import com.fg.project01.repository.enums.DayNameEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Service
public class TodayService {
    private static final Logger LOG = LoggerFactory.getLogger(TodayService.class);

    public Date getToday() {
        LOG.info("Getting a day today");
        return new Date();
    }

    public String format(Date date) {
        if (date == null) {
            return null;
        }

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        return formatter.format(date);
    }

    public DayNameEnum toDayName(Date date) {
        if (date == null) {
            return DayNameEnum.INVALID;
        }

        DateFormat formatter = new SimpleDateFormat("EEE", Locale.ENGLISH);
        return DayNameEnum.fromString(formatter.format(date));
    }
}
